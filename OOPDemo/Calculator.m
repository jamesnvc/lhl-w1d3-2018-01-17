//
//  Calculator.m
//  OOPDemo
//
//  Created by James Cash on 17-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Calculator.h"

@implementation Calculator

- (NSInteger)addValueTo:(NSInteger)n
{
    return n + 42;
}

@end
