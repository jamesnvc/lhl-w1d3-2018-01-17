//
//  Calculator.h
//  OOPDemo
//
//  Created by James Cash on 17-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calculator : NSObject

- (NSInteger)addValueTo:(NSInteger)n;

@end
