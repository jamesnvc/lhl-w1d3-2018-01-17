//
//  AdSupportedCalculator.h
//  OOPDemo
//
//  Created by James Cash on 17-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Calculator.h"

@interface AdSupportedCalculator : Calculator

@property (nonatomic) NSString* sponsor;

- (instancetype)initWithSponsor:(NSString*)sponsor;

@end
