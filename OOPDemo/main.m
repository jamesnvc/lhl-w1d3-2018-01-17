//
//  main.m
//  OOPDemo
//
//  Created by James Cash on 17-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Calculator.h"
#import "AdSupportedCalculator.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Calculator *c = [[Calculator alloc] init];

        NSLog(@"Calculator gives %ld", [c addValueTo:7]);

        Calculator *adCalc = [[AdSupportedCalculator alloc] initWithSponsor:@"Squarespace"];
        NSLog(@"Ad Calculator gives %ld", [adCalc addValueTo:9]);

        AdSupportedCalculator *adCalc2 = [[AdSupportedCalculator alloc] init];
        NSLog(@"Ad Calculator gives %ld", [adCalc2 addValueTo:11]);
        adCalc2.sponsor = @"Casper";
        NSLog(@"Ad Calculator gives %ld", [adCalc2 addValueTo:11]);
        adCalc2.sponsor = nil;
        NSLog(@"Ad Calculator gives %ld", [adCalc2 addValueTo:11]);

    }
    return 0;
}
