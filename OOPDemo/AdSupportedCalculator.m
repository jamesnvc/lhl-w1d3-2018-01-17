//
//  AdSupportedCalculator.m
//  OOPDemo
//
//  Created by James Cash on 17-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "AdSupportedCalculator.h"

@interface AdSupportedCalculator ()

@property (nonatomic) NSString* privateStringProp;

@end

@implementation AdSupportedCalculator

- (instancetype)initWithSponsor:(NSString*)sponsor
{
    self = [super init];
    if (self) {
        _sponsor = sponsor;
    }
    return self;
}

- (void)setSponsor:(NSString*)sponsor
{
    //self.sponsor = sponsor; // [self setSponsor:sponsor]
    // don't need to check for nil
    _sponsor = [sponsor uppercaseString];
}

- (void)printSponsorMessage {
    if (self.sponsor) {
        NSLog(@"This calculation brought to you by %@", self.sponsor);
    } else {
        NSLog(@"Your ad here");
    }
}

- (NSInteger)addValueTo:(NSInteger)n
{
    [self printSponsorMessage];
    return [super addValueTo:n];
}

@end
